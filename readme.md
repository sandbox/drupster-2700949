JQ Simplyscroll integrates jquery simplyscroll module available from [jQuery Simplyscroll](http://logicbox.net/jquery/simplyscroll/) into JQ drupal module allowing us to use simplyscroll plugin anywhere in drupal by calling function jq_add('simplyscroll');
This plugin requires jquery version 1.2.6 minimum

Install instructions
Download simplyscroll plugin from its homepage at http://logicbox.net/jquery/simplyscroll/ inside this module directory and uncompress the same.
Rename uncompressed folder to jquery.simplyscrool, its directory structure should look like

sites/all/modules/contrib/jq_simplyscroll/jquery.simplyscroll if your contributed modules are placed in sites/all/modules/contrib directory.

Go to Jq adminiistration page at  admin/settings/jq and check that Simplyscroll plugin has been enabled.

add this plugin to any plugin by calling  jq_add('simplyscroll')